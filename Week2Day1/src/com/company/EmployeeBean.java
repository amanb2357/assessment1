package com.company;

public class EmployeeBean {
    public int id;
    public String name;
    private int salary;

    public EmployeeBean(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
    }

    public boolean equals(EmployeeBean obj) {
        if(obj.id == id && obj.name.equals(name) && obj.salary == salary)
            return true;
        return false;
    }

    @Override
    public String toString() {
        return "id " + id + " name " + name + " salary " + salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }
}
