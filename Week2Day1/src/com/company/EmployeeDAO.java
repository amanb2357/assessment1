package com.company;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EmployeeDAO {

    public List<EmployeeBean> readData() throws IOException, FileNotFoundException {
        List<EmployeeBean> list = new ArrayList<EmployeeBean>();
        BufferedReader fileReader = null;
        fileReader = new BufferedReader(new FileReader("C:\\Users\\amabhard\\Desktop\\employee.txt"));
        String line;
        while((line = fileReader.readLine()) != null)
        {
            String[] data = line.split(",");
            list.add(new EmployeeBean(Integer.parseInt(data[0]), data[1], Integer.parseInt(data[2])));
        }
        return list;
    }

    interface SalarySum {
        public float sum(List<EmployeeBean> list);
    }

    public float getTotSal(List<EmployeeBean> list)
    {
        //for lambda
        SalarySum summer = (list1) -> {float total = 0;
                for(EmployeeBean bean : list)
                    total += bean.getSalary();
                return total;};
        float total = summer.sum(list);
        return total;
        /*float total = 0;
        for(EmployeeBean bean : list)
            total += bean.getSalary();
        return total;*/
    }

    public int getCount(List<EmployeeBean> list, int salary)
    {
        int count = 0;
        for(EmployeeBean bean : list)
            if(bean.getSalary() == salary)
                count++;
        return count;
    }

    public EmployeeBean getEmployee(int id) throws IOException {
        List<EmployeeBean> list = null;
        list = readData();

        for(EmployeeBean bean : list)
            if(bean.getId() == id)
            {
                return bean;
            }
        return null;
    }
}
