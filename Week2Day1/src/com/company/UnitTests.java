package com.company;

import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UnitTests {

    @Test
    public void testReadData() throws IOException {
        EmployeeDAO employee = new EmployeeDAO();
        List<EmployeeBean> received = employee.readData();
        List<EmployeeBean> actual = new ArrayList<EmployeeBean>();
        actual.add(new EmployeeBean(1, "aa" , 2));
        actual.add(new EmployeeBean(2, "aaa" , 4));
        actual.add(new EmployeeBean(3, "aaaa" , 6));
        actual.add(new EmployeeBean(4, "aaaaa" , 8));
        actual.add(new EmployeeBean(5, "aaaaaa" , 10));
        actual.add(new EmployeeBean(6, "aaaaaaa" , 12));
        actual.add(new EmployeeBean(7, "aaaaaaaa" , 14));
        actual.add(new EmployeeBean(8, "aaaaaaaaa" , 16));
        actual.add(new EmployeeBean(9, "aaaaaaaaaa" , 18));
        actual.add(new EmployeeBean(10, "aaaaaaaaaaa" , 20));
        actual.add(new EmployeeBean(11, "aaaaaaaaaaaa" , 22));
        actual.add(new EmployeeBean(12, "aaaaaaaaaaaaa" , 24));
        boolean flag = true;
        for(int i = 0 ; i < received.size() ; i++)
            if(!received.get(i).equals(actual.get(i)))
            {
                flag = false;
                break;
            }
        Assert.assertEquals(flag , true);
    }

    @Test
    public void testGetTotSal() throws IOException {
        EmployeeDAO employee = new EmployeeDAO();
        List<EmployeeBean> employees = employee.readData();
        float totSal = employee.getTotSal(employees);
        Assert.assertEquals(totSal, 156, 1e-15);
    }

    @Test
    public void testGetCount() throws IOException {
        EmployeeDAO employee = new EmployeeDAO();
        List<EmployeeBean> employees = employee.readData();
        int count = employee.getCount(employees, 2);
        Assert.assertEquals(count, 1);
    }

    @Test
    public void testGetEmployeeBean() throws IOException {
        EmployeeDAO employee = new EmployeeDAO();
        List<EmployeeBean> employees = employee.readData();
        EmployeeBean bean = employee.getEmployee(1);
        boolean flag = bean.equals(new EmployeeBean(1, "aa", 2));
        Assert.assertEquals(flag, true);
    }

}
